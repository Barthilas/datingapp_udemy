import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../_models/user';
import { PresenceService } from './presence.service';

//services are Singleton, runs for the lifetime of app.
//Components are destroyed
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl = environment.apiUrl;
  //ReplaySubject observable, buffer object, anytime subscriber subcsribes emit the last value.
  private currentUserSource = new ReplaySubject<User>(1);
  //observable convention $
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient, private presence: PresenceService) { }

  login(model: any)
  {
    return this.http.post(this.baseUrl + 'account/login', model).pipe(
      map((response) => {
        const user = response as User;
        if (user) {
          this.setCurrentUser(user);
          this.presence.createHubConnection(user);
        }
      })
    )
  }

  register(model: any){
    return this.http.post(this.baseUrl + 'account/register', model).pipe(
      map((response)=>{
        const user = response as User;
        if(user)
        {
          this.setCurrentUser(user);
          this.presence.createHubConnection(user);
        }
        //return user; to debug
      })
    )
  }

  setCurrentUser(user: User)
  {
    user.roles = [];
    const roles = this.getDecodedToken(user.token).role;
    Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);
    localStorage.setItem('user', JSON.stringify(user))
    this.currentUserSource.next(user);
  }

  logout(){
    localStorage.removeItem('user');
    this.currentUserSource.next(undefined);
    this.presence.stopHubConnection();
  }

  getDecodedToken(token: string) {
    //get token payload
    return JSON.parse(atob(token.split('.')[1]));
  }
}
