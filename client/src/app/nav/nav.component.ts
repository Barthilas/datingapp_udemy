import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  model: any = {};
  //currentUser$!: Observable<User>; //get rid of TypeScript security of initialization !.

  //public for template use
  constructor(public accountService : AccountService, private router: Router, 
    private toastr: ToastrService) { }

  ngOnInit(): void {

  }

  login() {
    //console.log(this.model);
    this.accountService.login(this.model).subscribe(response => {
      console.log(response);
      this.router.navigateByUrl('/members');
    })/*, error => {
      console.log(error);
      this.toastr.error(error.error);
    });*/
  }

  logout(){
    this.accountService.logout();
    this.router.navigateByUrl('/');
  }

  /*
  getCurrentUser()
  {
    //you need to subscribe to observable
    this.accountService.currentUser$.subscribe(user => {
      this.loggedIn = !!user; //!! turns object to boolean null=false, something=true
    }, error => {
      console.log(error);
    })
  }
  */

}
