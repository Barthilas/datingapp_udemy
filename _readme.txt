https://github.com/TryCatchLearn/DatingApp

dotnet new sln (zaváděcí soubor..)
dotnet new webapi -o API
dotnet sln add API  (přidání solution do zaváděcího souboru)

#Git (klasika, CLI verze)
git status
git init
...commit
git remote add origin git@gitlab.com:Barthilas/datingapp_udemy.git

Entity FW
dotnet ef migrations add InitialCreate -o Data/Migrations (prepare DB scheme)
dotnet ef database update (from migration create actual DB)
dotnet ef database drop (delete table)
dotnet ef migrations remove (remove last migration)

API
dotnet (watch) run
dotnet dev-certs https --trust (pro https podporu, certifikát)

Angular
ng serve (start)
ng g c nav --skip-tests (create component named nav without tests)
ng g s account --skip-tests (create services named accound without tests)
ng g guard auth --skip-tests (route protector)
ng g m shared --flat (module file)
ng g interceptor --skip-tests (error interceptors)

components 
(ngSubmit) -> template to component
[(ngModel)] -> two way binding (banana in a box)
[] -> component to template

Shortcuts
CTRL+SHIFT+P (open various extensions, settings)
SHIFT+ALT+DOWN/UP (up/down and copy current line)

SITES
https://www.json-generator.com/
Bootstrap, Bootswatch (themes)
Cloudinary, free cloud up to 10GB

(OMNISHARP, INTELISENSE NOT WORKING)
Ctrl + Shift + p
Write "OmniSharp: Select Project" and press Enter.
Choose the solution workspace entry.
