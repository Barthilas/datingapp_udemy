using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Data;
using API.DTOs;
using API.Entities;
using API.Extensions;
using API.Helpers;
using API.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Authorize]
    public class UsersController : BaseApiController
    {
        //private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOFWork;

        private readonly IPhotoService _photoService;
        public UsersController(IUnitOfWork unitOFWork, IMapper mapper,
        IPhotoService photoService) //DataContext context
        {
            _unitOFWork = unitOFWork;
            _photoService = photoService;
            _mapper = mapper;

        }

        // [Authorize(Roles = "Admin")]
        [HttpGet]
        //or return list, but it offers too many features.
        public async Task<ActionResult<IEnumerable<MemberDto>>> GetUsers([FromQuery] UserParams userParams)
        {
            /*
            var users = await _unitOFWork.UserRepository.GetUserAsync();
            var usersToReturn = _mapper.Map<IEnumerable<MemberDto>>(users);
            */
            // var user = await _unitOFWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());
            var gender = await _unitOFWork.UserRepository.GetUserGender(User.GetUsername());
            userParams.CurrentUsername = User.GetUsername();
            if (string.IsNullOrEmpty(userParams.Gender))
            {
                userParams.Gender = gender == "male" ? "female" : "male";
            }

            var users = await _unitOFWork.UserRepository.GetMembersAsync(userParams);

            Response.AddPaginationHeader(users.CurrentPage, users.PageSize,
             users.TotalCount, users.TotalPages);

            return Ok(users);
        }
        [HttpGet("{username}", Name = "GetUser")]
        //api/users/3
        public async Task<ActionResult<MemberDto>> GetUser(string username)
        {
            /*
            var user = await _unitOFWork.UserRepository.GetUserByUsername(username);
            return _mapper.Map<MemberDto>(user);
            */
            var currentUsername = User.GetUsername();
            var user = await _unitOFWork.UserRepository.GetMemberAsync(username, isCurrentUser: currentUsername==username);
            return user;
        }
        [HttpPut]
        public async Task<ActionResult> UpdateUser(MemberUpdateDto memberUpdateDto)
        {
            var username = User.GetUsername();
            var user = await _unitOFWork.UserRepository.GetUserByUsernameAsync(username);

            //user.City = memberUpdateDto.city.. - automapper
            _mapper.Map(memberUpdateDto, user);
            _unitOFWork.UserRepository.Update(user);
            if (await _unitOFWork.Complete()) return NoContent();

            return BadRequest("Failed to update user");
        }
        [HttpPut("set-main-photo/{photoId}")]
        public async Task<ActionResult> SetMainPhoto(int photoId)
        {
            var user = await _unitOFWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());
            var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);

            if (photo.IsMain) return BadRequest("This is already your main photo");

            var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);
            if (currentMain != null) currentMain.IsMain = false;
            photo.IsMain = true;

            if (await _unitOFWork.Complete()) return NoContent();

            return BadRequest("Failed to set main photo");
        }
        [HttpDelete("delete-photo/{photoId}")]
        public async Task<ActionResult> DeletePhoto(int photoId)
        {
            var user = await _unitOFWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());
            var photo = user.Photos.FirstOrDefault(x => x.Id == photoId);
            if (photo == null) return NotFound();
            if (photo.IsMain) return BadRequest("You cannot delete your main photo");
            if (photo.PublicId != null)
            {
                var result = await _photoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null) return BadRequest(result.Error.Message);
            }

            user.Photos.Remove(photo);
            if (await _unitOFWork.Complete()) return Ok();

            return BadRequest("Failed to delete the photo");
        }
        [HttpPost("add-photo")]
        public async Task<ActionResult<PhotoDto>> AddPhoto(IFormFile file)
        {
            var user = await _unitOFWork.UserRepository.GetUserByUsernameAsync(User.GetUsername());
            var result = await _photoService.AddPhotoAsync(file);
            if (result.Error != null) return BadRequest(result.Error.Message);

            var photo = new Photo
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId
            };

            //Approve photos now
            // if (user.Photos.Count == 0)
            // {
            //     photo.IsMain = true;
            // }

            user.Photos.Add(photo);
            if (await _unitOFWork.Complete())
            {
                //return _mapper.Map<PhotoDto>(photo);
                //Add location header on the response to the URI that would get that resource (basically route where to find the photo)
                return CreatedAtRoute("GetUser", new { username = user.UserName }, _mapper.Map<PhotoDto>(photo));
            }

            return BadRequest("Problem adding photo");

        }
    }
}
